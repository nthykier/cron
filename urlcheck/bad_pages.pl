#!/usr/bin/perl -w
# only include pages for which at least one error was reported

use strict;

my $strn = $ARGV[0];
my $line = '';
my $out = '';
if ($strn eq 'other') {
	$strn = '[^/]+\n';
}
else {
	$strn = "$strn/";
}

while (<STDIN>) {
	if (/^Looking/ and ($line ne '')) {
		# print $line;
		if ($line =~ m,^Looking into http://www.debian.org/$strn,) {
			$out .= $line;
		}
		$line = $_;
	}
	else {
		$line .= $_;
	}
}

if ($out ne '') {
	print <<END;
This is a list of bad links found in pages in this directory or
file on the Debian web site. Please fix them.

Note that not all URLs listed are bad. The error message should tell
you why the URL was flagged. In particular, in the interest of time,
the program times out faster than some very slow connections need to
finish the request.

END
	print $out;
}
